from time import time

from check import *
from methods import *


# -----------------------------------------------------------------------------
# Compare the performances
# -----------------------------------------------------------------------------
def compare(n, *functions):
    print()
    print("Comparison:")
    for function in functions:
        print(">", function.__name__)
        start = time()
        try:
            result = function(n)
            # print("Result:", result)
        except RuntimeError as error:
            print("Error:", error)
        print("Time:", "{:.3E}".format(time() - start), "s")
        print()


# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == "__main__":

    # n = 200000
    n = 40
    check_fibonacci_argument(n)

    compare(n,
        compute_fibonacci_naively,
        compute_fibonacci_with_memory,
        compute_fibonacci_iteratively,
        compute_fibonacci_advanced,
    )