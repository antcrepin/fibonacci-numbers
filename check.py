# -----------------------------------------------------------------------------
# Verify the argument n
# A nonnegative integer is expected
# -----------------------------------------------------------------------------
def check_fibonacci_argument(n):
    if not isinstance(n, int):
        raise TypeError("Expected argument type: {} "
            "(argument passed: {})".format(int, type(n)))
    if n < 0:
        raise ValueError("Expected a non-negative integer "
            "(argument passed: {})".format(n))