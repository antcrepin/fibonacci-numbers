from time import time

from check import *
from methods import compute_fibonacci_sequence


# -----------------------------------------------------------------------------
# Compute and print the Fibonacci sequence
# -----------------------------------------------------------------------------
def compute_sequence(n):
    print()
    print("Iterates from 0 to {}".format(n))
    print()
    try:
        sequence = compute_fibonacci_sequence(n)
        print("i\tF_i")
        for (i, val) in enumerate(sequence):
            print("{}\t{}".format(i, val))
    except RuntimeError as error:
        print("Error:", error)
    print()


# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == "__main__":

    n = 500
    check_fibonacci_argument(n)

    compute_sequence(n)
    