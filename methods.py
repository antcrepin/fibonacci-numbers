# -----------------------------------------------------------------------------
# Method 1
# Based on the definition
# Recursive
# Exponential complexity
# compute_fibonacci_naively(n) equals 2*F_{n+1}-1 calls to the function
# compute_fibonacci_naively
# -----------------------------------------------------------------------------
def compute_fibonacci_naively(n):
    if n < 2:
        return n
    return compute_fibonacci_naively(n-1) + compute_fibonacci_naively(n-2)


# -----------------------------------------------------------------------------
# Method 2
# Improve efficiency by memorizing the iterates that already have been
# calculated
# Sequential
# Linear complexity
# -----------------------------------------------------------------------------
def compute_fibonacci_sequence(n):
    # sequence initialized with [0] if n = 0; [0, 1] otherwise
    sequence = list(range(min(n, 1) + 1))
    if n >= 2:
        for i in range(2, n+1):
            # compute sequence[i] = F_i
            sequence.append(sequence[i-1] + sequence[i-2])
    return sequence

def compute_fibonacci_with_memory(n):
    sequence = compute_fibonacci_sequence(n)
    # return last element = F_n
    return sequence[-1]


# -----------------------------------------------------------------------------
# Method 3
# Based on the matrix formula involving (F_{n+1}, F_n) and (F_n, F_{n-1})
# Sequential
# Linear complexity
# -----------------------------------------------------------------------------
def compute_fibonacci_iteratively(n):
    current_iterate, next_iterate = 0, 1
    for i in range(n):
        # i from 0 to n-1
        # after iter i: (current_iterate, next_iterate) = (F_{i+1}, F_{i+2})
        current_iterate, next_iterate = \
            next_iterate, current_iterate + next_iterate
    return current_iterate


# -----------------------------------------------------------------------------
# Method 4
# Based on the formula involving (F_{n+1}, F_n) and (F_{n//2}, F_{n//2 + 1}))
# Recursive
# Logarithmic complexity
# -----------------------------------------------------------------------------
def compute_fibonacci_advanced_recursion(n):
    # return (F_n, F_{n+1})
    if n == 0:
        return 0, 1
    # a = F_{n//2}, b = F_{n//2 + 1}
    a, b = compute_fibonacci_advanced_recursion(n//2)
    # u = F_{2 * (n//2)}, v = F_{2 * (n//2) + 1}
    u = (2*b - a)*a
    v = a**2 + b**2
    if n % 2 == 0:
        return u, v
    return v, u + v

def compute_fibonacci_advanced(n):
    return compute_fibonacci_advanced_recursion(n)[0]