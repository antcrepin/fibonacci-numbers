# Fibonacci numbers

Several methods to compute Fibonacci numbers.

More details about the methods are available [here](https://antcrepin.gitlab.io/posts/fibonacci-numbers).

## Prerequisite

- Install [Python](https://www.python.org/downloads/) v3.5 or higher, and the following packages
  - matplotlib
  - seaborn
  - nump

## User guide

### Comparison of methods

```
python comparison.py
```

### Golden ratio approximation plot

```
python plot_golden_ratio_approximation.py
```

<p align="center">
  <img src="./plots/golden_ratio_approximation.png" width="66%">
</p>

### Golden spiral approximation plot

```
python plot_golden_spiral_approximation.py
```

<p align="center">
  <img src="./plots/golden_spiral_approximation.png" width="90%">
</p>

## License

This project is licensed under the terms of the MIT license.