import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

from check import *
from methods import compute_fibonacci_sequence


# -----------------------------------------------------------------------------
# Golden ratio approximation
# -----------------------------------------------------------------------------
def plot_golden_ratio_approximation(n):
    # check value of the argument
    if n < 2:
        raise ValueError("Argument n should be larger than 2")
    # plot exact value of the golden ratio
    GOLDEN_RATIO = (1 + 5**0.5)/2
    plt.axhline(GOLDEN_RATIO, color = "red",
        label = r'$\varphi = \left(1+\sqrt{5}\right)/2$', linestyle = "--")
    # compute F_{i+1}/F_i with i from 1 to n-1
    sequence = compute_fibonacci_sequence(n)
    ratios = np.array(sequence[2:]) / np.array(sequence[1:-1])
    # plot
    plt.plot(list(range(1, n)), ratios,
        color = "blue", label = r"$F_{n+1}/F_n$")
    plt.xlabel(r"$n$")
    plt.title(r"Approximation of $\varphi$")
    plt.legend()
    plt.savefig("plots/golden_ratio_approximation.png")
    plt.show()


# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == "__main__":

    n = 15
    check_fibonacci_argument(n)

    plot_golden_ratio_approximation(n)